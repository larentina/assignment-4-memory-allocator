#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
void malloc_data(){
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    void *ptr = _malloc(23);
    assert(ptr);
    _free(ptr);
    debug_heap(stdout, heap);
    heap_term();

}
void free_one_malloc_block(){

    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(23);
    assert(ptr1);

    void* ptr2 = _malloc(24);
    assert(ptr2);

    void* ptr3 = _malloc(25);
    assert(ptr3);
    debug_heap(stdout, heap);

    _free(ptr1);
    struct block_header* header_ptr1 = (struct block_header *) (ptr1 - offsetof(struct block_header, contents));
    assert(header_ptr1->is_free);
    debug_heap(stdout, heap);
    _free(ptr2);
    _free(ptr3);
    heap_term();

}
void free_two_malloc_block(){
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug_heap(stdout, heap);

    void* ptr1 = _malloc(23);
    assert(ptr1);

    void* ptr2 = _malloc(24);
    assert(ptr2);

    void* ptr3 = _malloc(25);
    assert(ptr3);
    debug_heap(stdout, heap);
    _free(ptr2);
    _free(ptr1);

    debug_heap(stdout, heap);

    _free(ptr3);
    heap_term();
}
void heap_grow(){
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug_heap(stdout, heap);
    void* ptr1 = _malloc(HEAP_SIZE*2);
    assert(ptr1);
    _free(ptr1);
    heap_term();
}
void heap_grow_trouble(){
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *ptr1 = map_pages(HEAP_START + HEAP_SIZE, REGION_MIN_SIZE, MAP_FIXED);
    void *ptr2 = _malloc(HEAP_SIZE+1);
    assert(ptr2);
    debug_heap(stdout, heap);
    assert(ptr1!=ptr2);
    _free(ptr1);
    heap_term();

}
int main()
{
   malloc_data();
    free_one_malloc_block();
    free_two_malloc_block();
    heap_grow();
    heap_grow_trouble();
    return 0;
}
